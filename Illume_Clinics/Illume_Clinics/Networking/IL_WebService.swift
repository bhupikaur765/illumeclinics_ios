//
//  IL_WebService.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 17/09/18.
//  Copyright © 2018 JaiWebSoft. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
import AlamofireObjectMapper
import ObjectMapper

class IL_WebService: NSObject {
    var loginVC  = ViewController()
    
    func loginAPI(email:String, password:String, view: ViewController) {
        
        let parameters: Parameters = [
            "email": email,
            "password": password
        ]
    
        Alamofire.request(NetworkConstant.SIGNIN, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: [:]).responseObject() {
            (response: DataResponse<LoginDetailsModel>)  in
            switch response.result {
            case .success:
                self.loginVC.setLoginDetails(loginDetails: response.result.value!, view : view)
            case .failure(let error):
                self.loginVC.showError(error:error as NSError, view : view)
                print(error)
            }
        }
    }
    
    func getProfileDetailsAPI() {
//        PSWD0086881
//        imkrishna53@gmail.com
//        a1@a.com
//        PSWD0014905
        let parameters: Parameters = [
            "email": "imkrishna53@gmail.com",
            "password": "PSWD0086881"
        ]
        
        Alamofire.request("http://admin.illumeclinics.com/illumeclinics/Api/login", method: .post, parameters: parameters).responseJSON { response in
            switch response.result {
            case .success:
                print("Validation Successful")
                if let json = response.result.value {
                    print("JSON: \(json)") // serialized json response
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func imageUpload() {
        let image = UIImage.init(named: "imageToUpload")
        //let imgData = UIImageJPEGRepresentation(image!, 0.2)!
        let imageData = image?.jpegData(compressionQuality: 0.2)
        let params: Parameters = ["visit_id": "1"]
        let header = [
            "Content-Type" : "application/json; charset=utf-8",
            "Accept":"application/json"
        ]

        Alamofire.upload(multipartFormData:
            {
                (multipartFormData) in
                multipartFormData.append(imageData!, withName: "image", fileName: "sign.jpeg", mimeType: "image/jpeg")
                for (key, value) in params
                {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
        }, to:"http://admin.illumeclinics.com/illumeclinics/Api/upload_image",headers:header)
        { (result) in
            switch result {
            case .success(let upload,_,_ ):
                upload.uploadProgress(closure: { (progress) in
                    //Print progress
                })
                upload.responseJSON
                    { response in
                        //print response.result
                        if response.result.value != nil
                        {
                                print("DATA UPLOAD SUCCESSFULLY")
                        }
                }
            case .failure(let encodingError):
                break
            }
        }
    }
}
