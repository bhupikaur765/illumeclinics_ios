//
//  DashboardCell.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 05/03/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {

    @IBOutlet weak var patientIdLbl: UILabel!
    @IBOutlet weak var ticketStatusLbl: UILabel!
    @IBOutlet weak var patientNameLbl: UILabel!
    @IBOutlet weak var patientProblemLbl: UILabel!
    @IBOutlet weak var patientSymptomsLbl: UILabel!
    @IBOutlet weak var consultantDocLbl: UILabel!
    @IBOutlet weak var dateAndTimeLbl: UILabel!
    @IBOutlet weak var borderView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        borderView.layer.cornerRadius = 5.0
        borderView.layer.masksToBounds = false
        borderView.layer.shadowColor = UIColor.gray.cgColor
        borderView.layer.shadowOpacity = 1
        borderView.layer.shadowOffset = CGSize.zero
        borderView.layer.shadowRadius = 3
    }
    
}
