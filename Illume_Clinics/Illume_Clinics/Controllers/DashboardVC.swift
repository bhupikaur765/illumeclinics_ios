//
//  DashboardVC.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 24/02/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit

class DashboardVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var dashboardTableView: UITableView!
    var pageIndex: Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set automatic dimensions for row height
        dashboardTableView.rowHeight = UITableView.automaticDimension
        dashboardTableView.estimatedRowHeight = UITableView.automaticDimension
        dashboardTableView.register(UINib(nibName: "DashboardCell", bundle: nil), forCellReuseIdentifier: "DashboardCell")
        dashboardTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Dashboard"
    }
    
    /*
     // MARK: - Table View Delegates
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3;
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell", for: indexPath) as! DashboardCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        return cell
    }
    
    
}
