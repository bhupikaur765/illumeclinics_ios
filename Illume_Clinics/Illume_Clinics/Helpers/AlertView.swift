//
//  AlertView.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 17/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation
import UIKit

class AlertView: NSObject {//This is shared class
    static let sharedInstance = AlertView()
    
    func alert(view: UIViewController, title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: { action in
        })
        alert.addAction(defaultAction)
        DispatchQueue.main.async(execute: {
            view.present(alert, animated: true)
        })
    }
    
    private override init() {
    }
    
    func alertWindow(title: String, message: String) {
        DispatchQueue.main.async(execute: {
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindow.Level.alert + 1
            
            let alert2 = UIAlertController(title: title, message: message, preferredStyle: .alert)
            let defaultAction2 = UIAlertAction(title: "OK", style: .default, handler: { action in
            })
            alert2.addAction(defaultAction2)
            
            alertWindow.makeKeyAndVisible()
            
            alertWindow.rootViewController?.present(alert2, animated: true, completion: nil)
        })
    }
    
    func askConfirmation (title:String, message:String, view:UIViewController, completion:@escaping (_ result:Bool) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        view.present(alert, animated: true, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            completion(true)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            completion(false)
        }))
    }
}


