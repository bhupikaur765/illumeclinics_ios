//
//  AddNewVisitHeaderView.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 19/03/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit

protocol AddNewVisitHeaderDelegate {
    func indexForSegmentControlSelected(integer:NSInteger)
}
class AddNewVisitHeaderView: UIView {
    
    @IBOutlet weak var segmentForView: UISegmentedControl!

    @IBOutlet weak var textFieldForView: UITextField!
    
    @IBOutlet weak var segmentControlBottomConstraint: NSLayoutConstraint!
    
    var addNewVisitDelegate : AddNewVisitHeaderDelegate!
    
    override func draw(_ rect: CGRect) {
        textFieldForView.rightViewMode = UITextField.ViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "information_icon.png")
        imageView.image = image
        textFieldForView.rightView = imageView
        
    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        if segmentForView.selectedSegmentIndex == 0 {
            textFieldForView.isHidden = false
        }
        else {
            textFieldForView.isHidden = true
        }
        addNewVisitDelegate.indexForSegmentControlSelected(integer: segmentForView.selectedSegmentIndex)
    }
    
    
}
