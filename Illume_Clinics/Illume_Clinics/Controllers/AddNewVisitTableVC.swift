//
//  AddNewVisitTableVC.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 05/03/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit

class AddNewVisitTableVC: UITableViewController, AddNewVisitHeaderDelegate {
    @IBOutlet var headerView: AddNewVisitHeaderView!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var isOldPatient:Bool = true
    
    let placeholderArrayForTextField : Array = ["PATIENT NAME","MOBILE NUMBER","AGE", "EMAIL ID","REFERRED BY", "REFERRED TO", "ADDRESS"]
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        // Set automatic dimensions for row height
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = UITableView.automaticDimension
        self.tableView.register(UINib(nibName: "IL_TextFieldCell", bundle: nil), forCellReuseIdentifier: "IL_TextFieldCell")
        self.tableView.register(UINib(nibName: "IL_ButtonCell", bundle: nil), forCellReuseIdentifier: "IL_ButtonCell")
        self.tableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tableView.tableHeaderView = headerView;
        let backgroundImage = UIImage(named: "splash_logo.png")
        let imageView = UIImageView(image: backgroundImage)
        imageView.contentMode = UIView.ContentMode.scaleAspectFit;
        self.tableView.backgroundView = imageView
        headerView.addNewVisitDelegate = self
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isOldPatient {
            return 0;
        }
        else {
            return placeholderArrayForTextField.count + 1
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == placeholderArrayForTextField.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "IL_ButtonCell", for: indexPath) as! IL_ButtonCell
            return cell
        }
        else {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IL_TextFieldCell", for: indexPath) as! IL_TextFieldCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.textFieldForCell.placeholder = placeholderArrayForTextField[indexPath.row]
        return cell
        }
    }
    
    
    // Header View Delegate
    func indexForSegmentControlSelected(integer: NSInteger) {
        view.endEditing(true)
        if integer == 0 {
            isOldPatient = true
        }
        else {
            isOldPatient = false
        }
        self.tableView.reloadData()
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
