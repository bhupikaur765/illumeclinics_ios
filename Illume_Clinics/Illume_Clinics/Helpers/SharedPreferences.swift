//
//  SharedPreferences.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 17/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation
struct Defaults {
    
    static let (nameKey, consultantDoctorKey, userTypeKey, doctorTypeKey) = ("name", "consultantDoctor","user_type","doctor_type")
    static let userSessionKey = "Illume_Clinics_Session"
    
    struct Model {
        var name: String?
        var consultantDoctor: String?
        var userType: String?
        var doctotType : String?
        
        
        init(_ json: [String: String]) {
            self.name = json[nameKey]
            self.consultantDoctor = json[consultantDoctorKey]
            self.userType = json[userTypeKey]
            self.doctotType = json[doctorTypeKey]
        }
    }
    
    static var saveNameAndConsultantDoctorAndUserType = { (name: String, consultantDoctor: String, userType: String ,doctorType:String) in
        UserDefaults.standard.set([nameKey: name, consultantDoctorKey: consultantDoctor, userTypeKey: userType, doctorTypeKey : doctorType], forKey: userSessionKey)
    }
    
    static var getNameAndConsultantDoctor = { _ -> Model in
        return Model((UserDefaults.standard.value(forKey: userSessionKey) as? [String: String]) ?? [:])
    }(())
    
    static func clearUserData(){
        UserDefaults.standard.removeObject(forKey: userSessionKey)
}
}
