//
//  ErrorModel.swift
//  Illume_Clinics
//
//  Created by PRIMOKO-WS-MBP-001 on 28/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper

class ErrorModel: Mappable {
    var message: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        message <- map["message"]
    }
}
