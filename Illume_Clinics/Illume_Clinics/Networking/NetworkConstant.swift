//
//  NetworkConstant.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 14/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation

import UIKit

class NetworkConstant: NSObject {
    
    static let ROOT = "http://api.illumeclinics.com/illumeclinics/Api/"
    static let SIGNIN = ROOT + "login"
    static let DashboardListClinic = ROOT + "/"
    
}
