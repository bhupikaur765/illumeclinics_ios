//
//  ViewController.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 09/02/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


class ViewController: UIViewController {
    @IBOutlet weak var usernameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var signInBtn: UIButton!
    
    var webService: IL_WebService!
    var loginDetails: LoginDetailsModel!
    let loader = AndroidLoaderView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sign In"
        webService = IL_WebService()
       // webService.imageUpload()
    }
    
    func setLoginDetails(loginDetails:LoginDetailsModel, view: ViewController) {
        self.loginDetails = loginDetails;
        if(self.loginDetails.userId == nil) {
            AlertView.sharedInstance.alert(view: view, title: "Alert", message: self.loginDetails.message!)
        }
        else {
            Navigator.onMoveToDashboard(view: view)
        }
        view.loader.hideLoaderFromView()
        view.view.isUserInteractionEnabled = true
    }
    
    func showError(error:NSError,view : ViewController) {
        view.loader.hideLoaderFromView()
        view.view.isUserInteractionEnabled = true
        AlertView.sharedInstance.alert(view: view, title: "Alert", message: error.localizedDescription)
        print("error")
    }
    
    @IBAction func signInBtnTapped(_ sender: Any) {
        // Testing Purpose
        //Navigator.onMoveToDashboard(view: self)
        
        self.loader.showLoaderOnView(currentView: self.view, text: "Loading...")
        self.view.isUserInteractionEnabled = false
        if ((usernameTF.text?.count)! > 0 && (passwordTF.text?.count)! > 0)  {
            webService.loginAPI(email: usernameTF.text!, password: passwordTF.text!, view:self)
        }
        else {
            loader.hideLoaderFromView()
           self.view.isUserInteractionEnabled = true
        }
    }
}

