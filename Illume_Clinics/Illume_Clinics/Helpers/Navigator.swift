//
//  Navigator.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 14/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation
import UIKit

class Navigator: NSObject {
    
    static func onMoveToDashboard(view : UIViewController){
        let topVC = topMostController()
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "revealVC") as? SWRevealViewController
        topVC.present(vc!, animated: true, completion: nil)
    }
    
    
//    static func onMoveToSignIn(view : UIViewController){
//        let vc: UIViewController? = view.storyboard?.instantiateViewController(withIdentifier: "viewSignIn")
//        if let aVc = vc {
//            view.navigationController?.pushViewController(aVc, animated: true)
//        }
//    }
    
    
    
    
//    static func onMoveToSignUpV(view : UIViewController){
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//        let vc: UIViewController? = storyboard.instantiateViewController(withIdentifier: "viewSignUp")
//        if let aVc = vc {
//            view.present(aVc, animated: true, completion: nil)
//        }
//        else{
//            print("Error")
//        }
//    }
}

func topMostController() -> UIViewController {
    var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
    while (topController.presentedViewController != nil) {
        topController = topController.presentedViewController!
    }
    return topController
}
