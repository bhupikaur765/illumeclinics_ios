//
//  IL_TextFieldCell.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 05/03/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class IL_TextFieldCell: UITableViewCell {

    @IBOutlet weak var textFieldForCell: SkyFloatingLabelTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
