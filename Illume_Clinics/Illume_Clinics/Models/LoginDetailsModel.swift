//
//  LoginDetailsModel.swift
//  Illume_Clinics
//
//  Created by Bhupinder on 14/04/19.
//  Copyright © 2019 JaiWebSoft. All rights reserved.
//

import Foundation
import ObjectMapper
import AlamofireObjectMapper

//class LoginDetailsModel: Mappable {
//    var name: String?
//    var userId: String?
//    var userType: String?
//    var doctorType: String?
//    var error : [Error]?
//
//    required init?(map: Map){
//
//    }
//
//    func mapping(map: Map) {
//        name <- map["username"]
//        userId <- map["user_id"]
//        userType <- map["user_type"]
//        doctorType <- map["doctor_type"]
//        error <- map["error"]
//    }
//}



struct LoginDetailsModel {
    var name: String?
    var userId: String?
    var userType: String?
    var doctorType: String?
    var message:String?
}

extension LoginDetailsModel: Mappable {
    
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        name <- map["data.username"]
        userId <- map["data.user_id"]
        userType <- map["data.user_type"]
        doctorType <- map["data.doctor_type"]
        message <- map["error.message"]
        
//        let error: [Error]?
//        error <- map["error"]
//
//        if let messageStr = error?.first(where: {$0.message == "message"})?.message {
//            message = messageStr
//        }
    }
}

struct Error {
    var message = ""
}

extension Error: Mappable {
    init?(map: Map) {
    }
    
    mutating func mapping(map: Map) {
        message <- map["message"]
    }
}

